<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eengineering');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ltD:TxjIuy]4bXe>dDPqdzl7-=XN7C8/vT5(BIq{1qg~p=kC=z9q#xYuft2.B$-p');
define('SECURE_AUTH_KEY',  '/QS6b?=SmV*:hb$=. gaWHH%3~EcP%h0@AVWYvAeQ[u()#h[#) iQfs9sPXjCPhb');
define('LOGGED_IN_KEY',    '8ew6<=Msh7`R*u&,cmVP?oKbthkbNW4=N<XzyR/AM0PZk}Pucs!QC)L}Uc@`%}>i');
define('NONCE_KEY',        'z.,2Ap(O[A_[c~1N=_LF4BV=RxS#B^x?4][,O#WEGO8J;<H2nJi<GAJ3(6lpo1Cj');
define('AUTH_SALT',        ':6fkjZI.dypG^5ZpWpj`Ph$][GaPL{  2zaZM$a<8/,!qk,78&lMi]EU]t-vHb)m');
define('SECURE_AUTH_SALT', 'F/N>Su]>IUmZk#WrM/V]K,g33dy{&@<mRBh98ahuc}Pvl[T;O+Od%l47^-oVsPXe');
define('LOGGED_IN_SALT',   '=evgH6zh>h6(*$$v+S<QvT3:~t(k`_1,w72*eH;p<i*F41hN#AxA[jxWrhog8!VT');
define('NONCE_SALT',       '4#k8x;Z20(_76,XEY#y(cX)vk4OD~SNpzW|{#:b[9<,m,bxV hl+tPO` 5^b%^T/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
